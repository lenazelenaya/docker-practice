FROM openjdk:11
ADD target/docker-practice.jar docker-practice.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar","docker-practice.jar"]